import { NavLink, Route, Switch } from 'react-router-dom';
import GameContainer from './components/Game/GameContainer';
import ProfileContainer from './components/Profile/ProfileContainer';
import AuthContainer from './components/Auth/AuthContainer';

function App() {
  return (
    <div className="battleship">
      <nav>
        <NavLink to='profile'>Profile</NavLink>
        <NavLink to='game'>Game</NavLink>
      </nav>
      <Switch>
        <Route
          exact path='/'
          render={() => <AuthContainer/>}/>
        <Route
          path='/profile'
          render={() => <ProfileContainer/>}/>
        <Route
          path='/game'
          render={() => <GameContainer/>}/>
      </Switch>
    </div>
  );
}

export default App;
