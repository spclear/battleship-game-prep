class Field {
  constructor(field) {
    this._field = field;
  }

  get field() {
    return this._field;
  }
}

export default Field;