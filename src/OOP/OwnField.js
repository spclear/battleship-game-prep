import Field from './Field';
import Validation from './Validation';

// shipList: [length of a ship]: [ships left], e.g. {3: 2} means there are 2 ships of length 3 left;
// size: int – represents size of a chosen ship to place

export class OwnField extends Field {
  constructor({field, direction = 'horizontal', size = 1}) {
    super(field);

    this._validationMessage = '';
    this._shipsList = { 4: 1, 3: 2, 2: 3, 1: 4 };
    this._direction = direction;
    this._currentSize = size;
    this._isReady = false;
    this._shipsCoords = {
      4: [],
      3: [],
      2: [],
      1: []
    };
    this._validation = new Validation();
  }

  get isReady() { return this._isReady }

  get shipsList() { return this._shipsList }
  
  get size() { return this._currentSize }
  
  get direction() { return this._direction }
  
  get shipsCoords() {
    return this._isReady
      ? this._shipsCoords
      : new Error('You haven\'t placed all your ships!');
  }
  
  set size(num) {
    if (num > 4 || num < 1) {
      throw new Error('Incorrect size value. Must be between 1 and 4.');
    }
    if (!Number.isInteger(num)) {
      throw new Error('Incorrect size value. Must be integer.');
    }
    this._currentSize = num;
  }

  set direction(dir) {
    if (dir !== 'horizontal' && dir !== 'vertical') {
      return new Error('Invalid direction property');
    }
    this._direction = dir;
  }

  placeShip(y, x) {
    if (!this._validate(y, x)) {
      console.log(this._validationMessage);
      return false;
    }

    const newField = JSON.parse(JSON.stringify(this._field));
    const ship = [];

    switch (this._direction) {
      case 'horizontal': 
        for (let i = x; i < x + this._currentSize; i++) {
          newField[y][i] = 1;
          ship.push([y, i]);
        }
        break;
      case 'vertical':
        for (let i = y; i < y + this._currentSize; i++) {
          newField[i][x] = 1;
          ship.push([i, x]);
        }
        break;
      default:
        break;
    }
    
    this._addShip(ship);
    this._decreaseShipCount();
    this._checkReadyStatus();
    this._field = newField;
  }

  _addShip(ship) {
    const currSize = this._currentSize;
    this._shipsCoords[currSize].push(ship);
  }

  _checkReadyStatus() {
    if (this._validation.isEmpty(this._shipsList)) {
      this._isReady = true;
    }
  }

  _decreaseShipCount() {
    const size = this._currentSize;
    this._shipsList = {
      ...this._shipsList,
      [size]: this._shipsList[size] - 1
    }
  }

  _validate(y, x) {
    const {
      isEmpty,
      shipsOfSize,
      validateHorizontal,
      validateVertical
    } = this._validation;

    
    if (isEmpty(this._shipsList)) {
      this._isReady = true;
      this._validationMessage = 'You have no ships left!';
      return false;
    }
    
    if (!shipsOfSize(this._shipsList, this._currentSize)) {
      this._validationMessage = `You have no ships with size ${this._currentSize} left!`;
      return false;
    }

    switch (this._direction) {
      case 'horizontal':
        if (!validateHorizontal(this._field, y, x, this._currentSize)) {
          this._validationMessage = 'You can\'t fit your ship here!'
          return false;
        }
        break;
      case 'vertical':
        if (!validateVertical(this._field, y, x, this._currentSize)) {
          this._validationMessage = 'You can\'t fit your ship here!'
          return false;
        }
        break;
      default:
        break;
    }

    this._validationMessage = '';
    return true;
  }
}
