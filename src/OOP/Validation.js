class Validation {
  isEmpty(ships) {
    return Object.values(ships).every(item => !item);
  }

  shipsOfSize(ships, size) {
    return !!ships[size];
  }

  validateHorizontal(field, y, x, size) {
    // check if last cell of ~possible~ ship is IN the field (same for vertical)
    const lastCell = field[y][x + size - 1];
    if (lastCell === undefined) {
      return false;
    }
    
    // check if there are any ships  on ~possible~ placement cells
    // or on the closest cells (same for vertical)
    for (let i = x - 1; i <= x + size; i++) {
      const topCell = field[y + 1] && field[y + 1][i];
      const botCell = field[y - 1] && field[y - 1][i]
      if (field[y][i] || topCell || botCell) {
        return false;
      }
    }

    return true;
  }

  validateVertical(field, y, x, size) {
    const lastRow = field[y + size - 1];
    if (lastRow === undefined) {
      return false;
    }

    for (let i = y - 1; i <= y + size; i++) {
      if (field[i] === undefined) continue;
      if (field[i][x] || field[i][x - 1] || field[i][x + 1]) {
        return false;
      }
    }

    return true;
  }
}

export default Validation;