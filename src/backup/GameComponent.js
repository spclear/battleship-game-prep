import React, { useState } from 'react';
import BattleField from '../components/Game/BattleField/BattleField';
import styles from './game.module.css';
import genField from '../common/generators/genField';
import { isNotPlaced, validateHorizontal, validateVertical } from '../common/validation/gameValidation/placementValidation';

const Game = (props) => {
  const [direction, setDirection] = useState('horizontal');
  const [field, setField] = useState(genField());
  const [size, setSize] = useState(4);
  const [ships, setShips] = useState({ 1: 4, 2: 3, 3: 2, 4: 1 });

  const isEmpty = () => {
    return Object.values(ships).every(item => !item)
  };
  const sizeButtons = Object.keys(ships).map(item => {
    return (
      <button
        className={styles.setSize}
        onClick={() => setSize(+item)}
        disabled={!ships[item]}
        key={item}
      >
        Size: {item} Left: {ships[item]}
      </button>
    )
  })

  const placeShip = (y, x) => {
    if (isEmpty()) {
      return alert('You have no ships left!');
    }
    if (!isNotPlaced(field, y, x)) {
      return alert('You can\'t place your ship here!');
    }
    if (!ships[size]) {
      return alert(`You have no ships with size ${size} left!`);
    }

    const newField = JSON.parse(JSON.stringify(field));

    switch (direction) {
      case 'horizontal':
        if (!validateHorizontal(field, y, x, size)) {
          alert('You !!can\'t fit your ship here!');
          return;
        }
        for (let i = x; i < x + size; i++) {
          newField[y][i] = 1;
        }
        break;
      case 'vertical':
        if (!validateVertical(field, y, x, size)) {
          alert('You can\'t fit your ship here!');
          return;
        }
        for (let i = y; i < y + size; i++) {
          newField[i][x] = 1;
        }
        break;
      default:
        break;
    }
    
    setShips(ships => ({...ships, [size]: ships[size] - 1}))
    setField(newField);
  }

  const toggleDirection = () => {
    if (direction === 'horizontal') {
      setDirection('vertical');
    } else {
      setDirection('horizontal');
    }
  }

  return (
    <div className={styles.game}>
      <BattleField
        field={field}
        handleClick={placeShip}/>
      <div className={styles.options}>
        <button
          className={styles.toggleDirection}
          onClick={toggleDirection}>
          Change direction
        </button>
        <div className={styles.currOptions}>
          <div>Current direction: {direction}</div>
          <div>Current size: {size}</div>
        </div>
        <div className={styles.size}>
          {sizeButtons}
        </div>
      </div>
    </div>
  )
}

export default Game;