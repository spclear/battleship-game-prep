import React from 'react';
import styles from './forminput.module.css';

const FormInput = (props) => {
  const { type, name, inputRef, label } = props;

  return (
    <div className={styles.field}>
      <input
        type={type}
        autoComplete='off'
        placeholder=' '
        name={name}
        ref={inputRef}/>
      <label>
        <span>{label}</span>
      </label>
    </div>
  )
}

export default FormInput;