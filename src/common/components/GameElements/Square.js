import React from 'react';
import styles from './square.module.css';

const Square = ({ itemKey, clickHandler, hoverHandler, ...props }) => {
  const allowedClass = props.isAllowed ? styles.allowed : styles.notAllowed;
  return (
    <div
      className={styles.square + ' ' + allowedClass}
      key={itemKey}
      onClick={clickHandler}
      onMouseEnter={hoverHandler}/>
  )
}

export default Square;