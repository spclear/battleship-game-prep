const genField = () => {
  const field = [];
  for (let i = 0; i < 10; i++) {
    field[i] = new Array(10).fill(0);
  }
  return field;
}

export default genField;