export function validateHorizontal(field, y, x, size) {
  const lastCell = field[y][x + size - 1];
  if (lastCell === undefined) {
    return false;
  }

  for (let i = x - 1; i <= x + size; i++) {
    const topCell = field[y + 1] && field[y + 1][i];
    const botCell = field[y - 1] && field[y - 1][i]
    if (field[y][i] || topCell || botCell) {
      return false;
    }
  }

  return true;
}

export function validateVertical(field, y, x, size) {
  const lastRow = field[y + size - 1];
  if (lastRow === undefined) {
    return false;
  }

  for (let i = y - 1; i <= y + size; i++) {
    if (field[i] === undefined) continue;
    if (field[i][x] || field[i][x - 1] || field[i][x + 1]) {
      return false;
    }
  }

  return true;
}

export function isNotPlaced(field, y, x) {
  return !field[y][x];
}

