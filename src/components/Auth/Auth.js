import React from 'react';
import { useForm, Controller } from 'react-hook-form';
import { NavLink } from 'react-router-dom';
import FormInput from '../../common/components/FormElements/FormInput';
import styles from './auth.module.css';

const Auth = (props) => {
  const { register, control, handleSubmit } = useForm();
  const onSubmit = data => {
    console.log(data);
  }
  return (
    <div className={styles.auth}>
      
      <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
        <h1 className={styles.title}>Sign up</h1>
        
        <Controller
          name='email'
          control={control}
          defaultValue=''
          render={(props) => (
            <FormInput
              label='Email'
              name={props.name}
              type='text'
              inputRef={register}/>
          )}/>
        
        <Controller
          name='password'
          control={control}
          defaultValue=''
          render={(props) => (
            <FormInput
              label='Password'
              name={props.name}
              type='password'
              inputRef={register}/>
          )}/>

        <div className={styles.options}>
          <input
            id='remember'
            ref={register}
            type='checkbox'
            name='remember'
            defaultChecked={true} />
          <label htmlFor='remember'>
            Remember me
          </label>
        </div>
        
        <button>Sign in</button>
      </form>

      <div className={styles.register}>
        <span>New here?</span>
        <NavLink
          className={styles.registerLink}
          to='/registration'>
          Registration
        </NavLink>
      </div>
       
    </div>
  )
}

export default Auth;