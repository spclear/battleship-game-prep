import React from 'react';
import styles from './battlefield.module.css';
import Square from '../../../common/components/GameElements/Square';

const BattleField = (props) => {
  const createGrid = (field, handleClick) => {
    return field.map((item, i) => {
      return item.map((square, j) => {
        return <Square
          isAllowed={!square}
          key={10 * i + j}
          clickHandler={() => {
            handleClick(i, j);
          }}
        />;
      })
    })
  }

  return (
    <div className={styles.field}>
      {createGrid(props.field, props.handleClick)}
    </div>
  )
}

export default BattleField;