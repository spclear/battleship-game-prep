import React from 'react';
import styles from './controls.module.css';

const Controls = ({ size, direction, field, ...props }) => {
  const sizeButtons = Object.keys(field.shipsList).map(item => {
    return (
      <button
        className={styles.setSize}
        disabled={!field.shipsList[item]}
        key={item}
        onClick={() => props.changeSize(+item)}
      >
        Size: {item} Left: {field.shipsList[item]}
      </button>
    )
  })

  return (
    <div className={styles.controls}>
      <button
        className={styles.toggleDirection}
        onClick={props.toggleDirection}>
        Change direction
      </button>
      <div className={styles.currOptions}>
        <div>Current direction: {direction}</div>
        <div>Current size: {size}</div>
      </div>
      <div className={styles.size}>
        {sizeButtons}
      </div>
    </div>
  )
}

export default Controls;