import React, { useState } from 'react';
import BattleField from './BattleField/BattleField';
import styles from './game.module.css';
import genField from '../../common/generators/genField';
import { OwnField } from '../../OOP/OwnField';
import Controls from './Controls/Controls';

const Game = (props) => {
  const newField = genField();

  const [field] = useState(new OwnField({field: newField}));
  const [currGrid, setCurrGrid] = useState(field.field);
  const [direction, setDirection] = useState(field.direction);
  const [size, setSize] = useState(field.size);
  const [isReady, setIsReady] = useState(false);

  const toggleDirection = () => {
    if (field.direction === 'horizontal') {
      field.direction = 'vertical';
    } else {
      field.direction = 'horizontal';
    }
    setDirection(field.direction);
  }

  const changeSize = (item) => {
    field.size = item;
    setSize(field.size);
  }

  const placeShip = (y, x) => {
    field.placeShip(y, x);
    setIsReady(field.isReady);
    setCurrGrid(field.field);
  }
  
  return (
    <div className={styles.game}>
      <BattleField
        field={currGrid}
        handleClick={placeShip}
      />
      <Controls
        toggleDirection={toggleDirection}
        changeSize={changeSize}
        direction={direction}
        size={size}
        field={field}
      />
      <button
        className={styles.ready}
        disabled={!isReady}
        onClick={() => {
          if (isReady) {
            alert('Start game.');
            console.log(field.shipsCoords);
          } 
        }}  
      >
        READY
      </button>
    </div>
  )
}

export default Game;