const SET_READY_STATUS = 'SET_READY_STATUS';

const initialState = {
  isReady: false,
};

export const placementReducer = (state = initialState, action) {
  switch (action.type) {
    case SET_READY_STATUS:
      return {
        ...state,
        isReady: action.payload
      };
    default:
      return state;
  }
}

export const setReadyStatus = (isReady) => {
  return {
    type: SET_READY_STATUS,
    payload: isReady
  }
}
